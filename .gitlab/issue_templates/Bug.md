== BEGIN OF HEADER ==

*Thanks for filling a bug issue! Please fill the template and remove this header before submitting the issue.*

*Please check if any issue or merge request regarding your issue has not already been filled!*

== END OF HEADER ==

# Description of the issue

*Description comes here. Remove this line before submitting.*

# Steps to reproduce the problem

*Leave empty if you are unable to provide a pattern to reproduce the problem*

# Operating systems concerned

*Please select all boxes concerned by the issue. Remove this line before submitting.*

- [ ] Linux
- [ ] Mac
- [ ] Windows
- [ ] Other: *None*
- [ ] I don't know

# Node version

*Please provide the node version with `node -v`. Remove this line before submitting.*
