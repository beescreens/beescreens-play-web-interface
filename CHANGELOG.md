# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Added

- Proof of concept of a UI allowing the players to see the applications

## [0.0.1] - 2020-02-24

### Added

- Set up a complete pipeline with best pratices regarding:
  - Documentation
  - Contributions files
  - CI/CD
  - Unit testing
  - Building packages
  - Auditing
  - Linting
  - Scripts helpers

[0.0.1]: https://gitlab.com/beescreens/beescreens-play-web-interface/-/tags/0.0.1
