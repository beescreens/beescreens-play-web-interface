const tasks = arr => arr.join(' && ');

module.exports = {
  hooks: {
    'pre-commit': tasks([
      'npm run check-outdated-packages',
      'npm run audit-packages',
      'npm run sync-env',
      'npm run lint',
      'npm run test',
      'npm run build',
      'npm run lint-dockerfile',
    ]),
  },
};
