import React from 'react';
import { Application } from '@beescreens/beescreens';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import { Container, CssBaseline, Grid } from '@material-ui/core';

import { useRouter } from 'next/router';

import ApplicationCard from '../components/ApplicationCard';
import CustomAppBar from '../components/CustomAppBar';
import ConfigurationDialog from '../components/ConfigurationDialog';
import { withTranslation } from '../i18n';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      flexGrow: 1,
      padding: theme.spacing(2),
    },
  })
);

function Index({ applications }: any) {
  const classes = useStyles();
  const router = useRouter();

  const { showConfig } = router.query;

  const [open, setOpen] = React.useState(showConfig === 'true');

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <CustomAppBar openConfig={handleClickOpen} />
      <ConfigurationDialog
        showConfiguration={open}
        closeConfiguration={handleClose}
      />
      <Container component='main' maxWidth='lg' className={classes.container}>
        <CssBaseline />
        <Grid container justify='center' spacing={2}>
          {applications.map(
            ({
              name,
              description,
              gameTime,
              minimumPlayers,
              maximumPlayers,
              minimumScreens,
              maximumScreens,
              version,
              contact,
              logoUrl,
              homepageUrl,
              documentationUrl,
              endpointUrl,
            }: Application) => (
              <Grid item md={4} key={name}>
                <ApplicationCard
                  name={name}
                  description={description}
                  gameTime={gameTime}
                  minimumPlayers={minimumPlayers}
                  maximumPlayers={maximumPlayers}
                  minimumScreens={minimumScreens}
                  maximumScreens={maximumScreens}
                  version={version}
                  contact={contact}
                  logoUrl={logoUrl}
                  homepageUrl={homepageUrl}
                  documentationUrl={documentationUrl}
                  endpointUrl={endpointUrl}
                />
              </Grid>
            )
          )}
        </Grid>
      </Container>
    </>
  );
}

Index.getInitialProps = async () => {
  const applications = [
    {
      name: 'Drawing app',
      description: 'Dessine sur les murs !',
      gameTime: '3',
      minimumPlayers: '1',
      maximumPlayers: '1',
      minimumScreens: '1',
      maximumScreens: '1',
      version: '0.0.1',
      contact: 'contact@beescreens.ch',
      logoUrl:
        'https://images.unsplash.com/photo-1525909002-1b05e0c869d8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=975&q=80',
      homepageUrl: 'https://drawing-app.beescreens.ch',
      documentationUrl: 'https://gitlab.com/beescreens/drawing-app',
      endpointUrl: 'https://drawing-app-play.beescreens.ch',
    },
    {
      name: 'PlanetIO',
      description: 'Mange les planetes !',
      gameTime: '15',
      minimumPlayers: '1',
      maximumPlayers: '3',
      minimumScreens: '1',
      maximumScreens: '1',
      version: '0.0.1',
      contact: 'contact@beescreens.ch',
      logoUrl:
        'https://images.unsplash.com/photo-1465101162946-4377e57745c3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1957&q=80',
      homepageUrl: 'https://planetio-app.beescreens.ch',
      documentationUrl: 'https://gitlab.com/beescreens/planetio-app',
      endpointUrl: 'https://planetio-app-play.beescreens.ch',
    },
  ];

  return {
    namespacesRequired: ['common'],
    applications,
  };
};

export default withTranslation()(Index);
