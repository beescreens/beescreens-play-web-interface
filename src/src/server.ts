import express from 'express';
import next from 'next';
import nextI18NextMiddleware from 'next-i18next/middleware';

import NextI18NextInstance from './i18n';

const app = next({ dev: process.env.NODE_ENV !== 'production' });
const handle = app.getRequestHandler();

(async (): Promise<void> => {
  await app.prepare();
  const server = express();

  await NextI18NextInstance.initPromise;

  server.use(nextI18NextMiddleware(NextI18NextInstance));

  server.get('*', (req: any, res: any) => handle(req, res));

  await server.listen('63353');

  console.log('> Ready on http://localhost:63353');
})();
