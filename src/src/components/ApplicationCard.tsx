import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import {
  Button,
  Typography,
  CardMedia,
  CardContent,
  CardActions,
  Card,
} from '@material-ui/core';

import { withTranslation } from '../i18n';
import ApplicationDetails from './ApplicationDetails';

const useStyles = makeStyles(() =>
  createStyles({
    card: {
      maxWidth: 500,
    },
    media: {
      height: 200,
    },
  })
);

function ApplicationCard({
  t,
  name,
  description,
  gameTime,
  minimumPlayers,
  maximumPlayers,
  minimumScreens,
  maximumScreens,
  version,
  contact,
  logoUrl,
  homepageUrl,
  documentationUrl,
  endpointUrl,
}: any) {
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const playNow = () => {
    console.log(endpointUrl);
  };

  return (
    <div>
      <Card className={classes.card}>
        <CardMedia
          className={classes.media}
          image={logoUrl}
          title={`${name} logo`}
        />
        <CardContent>
          <Typography gutterBottom variant='h5' component='h2'>
            {name}
          </Typography>
          <Typography variant='body2' color='textSecondary' component='span'>
            {description}
          </Typography>
        </CardContent>
        <CardActions>
          <Button onClick={playNow}>{t('common:play-now')}</Button>
          <Button onClick={handleClickOpen}>{t('show-details')}</Button>
        </CardActions>
      </Card>
      <ApplicationDetails
        open={open}
        closeDetails={handleClose}
        playNow={playNow}
        name={name}
        description={description}
        gameTime={gameTime}
        minimumPlayers={minimumPlayers}
        maximumPlayers={maximumPlayers}
        minimumScreens={minimumScreens}
        maximumScreens={maximumScreens}
        version={version}
        contact={contact}
        homepageUrl={homepageUrl}
        documentationUrl={documentationUrl}
      />
    </div>
  );
}

ApplicationCard.getInitialProps = async (): Promise<any> => ({
  namespacesRequired: ['common', 'application-card'],
});

export default withTranslation('application-card')(ApplicationCard);
