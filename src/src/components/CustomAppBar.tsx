import React from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import { IconButton, Typography, AppBar, Toolbar } from '@material-ui/core';
import { Translate, Settings, Brightness4 } from '@material-ui/icons';

import { i18n, withTranslation } from '../i18n';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      flexGrow: 1,
      padding: theme.spacing(2),
    },
    grow: {
      flexGrow: 1,
    },
    title: {
      flexGrow: 1,
    },
  })
);

function CustomAppBar({ t, openConfig }: any) {
  const classes = useStyles();

  const switchTheme = () => {
    alert('TODO');
  };

  const switchLangages = () => {
    return i18n.changeLanguage(i18n.language === 'en' ? 'fr' : 'en');
  };

  return (
    <div className={classes.grow}>
      <AppBar position='static' color='inherit'>
        <Toolbar>
          <Typography className={classes.title} variant='h6' noWrap>
            {t('title')}
          </Typography>
          <IconButton aria-label='settings' onClick={openConfig}>
            <Settings />
          </IconButton>
          <IconButton aria-label='theme' onClick={switchTheme}>
            <Brightness4 />
          </IconButton>
          <IconButton aria-label='language' onClick={switchLangages}>
            <Translate />
          </IconButton>
        </Toolbar>
      </AppBar>
    </div>
  );
}

CustomAppBar.getInitialProps = async (): Promise<any> => ({
  namespacesRequired: ['common', 'app-bar'],
});

export default withTranslation('app-bar')(CustomAppBar);
