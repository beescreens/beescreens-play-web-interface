import React from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import { Button, Grid, Link, Typography, Dialog, Box } from '@material-ui/core';
import { withTranslation } from '../i18n';
import {
  CustomDialogTitle,
  CustomDialogContent,
  CustomDialogActions,
} from './CustomDialog';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    play: {
      margin: theme.spacing(3, 0, 3),
    },
  })
);

function ApplicationDetails({
  t,
  open,
  closeDetails,
  playNow,
  name,
  description,
  gameTime,
  minimumPlayers,
  maximumPlayers,
  minimumScreens,
  maximumScreens,
  version,
  contact,
  homepageUrl,
  documentationUrl,
}: any) {
  const classes = useStyles();

  return (
    <Dialog
      onClose={closeDetails}
      aria-labelledby='customized-dialog-title'
      open={open}
      fullWidth
      maxWidth='lg'
    >
      <CustomDialogTitle id='customized-dialog-title' onClose={closeDetails}>
        {`${name}'s details`}
      </CustomDialogTitle>
      <CustomDialogContent dividers>
        <Typography gutterBottom>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Typography>
                <Box fontWeight='fontWeightBold'>{t('name-label')}</Box>
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>{name}</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                <Box fontWeight='fontWeightBold'>{t('description-label')}</Box>
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>{description}</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                <Box fontWeight='fontWeightBold'>{t('game-time-label')}</Box>
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>{gameTime}</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                <Box fontWeight='fontWeightBold'>
                  {t('minimum-players-label')}
                </Box>
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>{minimumPlayers}</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                <Box fontWeight='fontWeightBold'>
                  {t('maximum-players-label')}
                </Box>
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>{maximumPlayers}</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                <Box fontWeight='fontWeightBold'>
                  {t('minimum-screens-label')}
                </Box>
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>{minimumScreens}</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                <Box fontWeight='fontWeightBold'>
                  {t('maximum-screens-label')}
                </Box>
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>{maximumScreens}</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                <Box fontWeight='fontWeightBold'>{t('version-label')}</Box>
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>{version}</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                <Box fontWeight='fontWeightBold'>{t('contact-label')}</Box>
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                <Link href={`mailto:${contact}`} color='inherit'>
                  {contact}
                </Link>
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                <Box fontWeight='fontWeightBold'>{t('homepage-url-label')}</Box>
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                <Link href={homepageUrl} color='inherit'>
                  {homepageUrl}
                </Link>
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                <Box fontWeight='fontWeightBold'>
                  {t('documentation-url-label')}
                </Box>
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                <Link href={documentationUrl} color='inherit'>
                  {documentationUrl}
                </Link>
              </Typography>
            </Grid>
          </Grid>
        </Typography>
      </CustomDialogContent>
      <CustomDialogActions>
        <Button
          variant='contained'
          fullWidth
          className={classes.play}
          onClick={playNow}
        >
          {t('common:play-now')}
        </Button>
      </CustomDialogActions>
    </Dialog>
  );
}

ApplicationDetails.getInitialProps = async (): Promise<any> => ({
  namespacesRequired: ['common', 'application-details'],
});

export default withTranslation('application-details')(ApplicationDetails);
