import React, { FormEvent, useEffect, useState } from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import {
  Button,
  Dialog,
  Grid,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from '@material-ui/core';
import { Cloud, Visibility, VisibilityOff, LockOpen } from '@material-ui/icons';

import fetch from 'node-fetch';
import { useRouter } from 'next/router';

import { withTranslation } from '../i18n';
import {
  CustomDialogTitle,
  CustomDialogContent,
  CustomDialogActions,
} from './CustomDialog';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(3),
    },
    submit: {
      margin: theme.spacing(3, 0, 3),
    },
  })
);

interface State {
  apiEndpoint: string | string[];
  apiEndpointError: boolean;
  apiJwt: string | string[];
  showJwt: boolean;
  applicationId: string | string[];
  applicationIdError: boolean;
  submit: boolean;
}

function ConfigurationDialog({
  t,
  closeConfiguration,
  showConfiguration,
}: any) {
  const classes = useStyles();
  const router = useRouter();

  const { apiEndpoint, showJwt, applicationId, submit } = router.query;

  const [configuration, setConfiguration] = useState<State>({
    apiEndpoint: apiEndpoint || process.env.BEESCREENS_API_ENDPOINT || '',
    apiEndpointError: false,
    apiJwt: '',
    showJwt: showJwt === 'true',
    applicationId,
    applicationIdError: false,
    submit: submit === 'true',
  });

  const handleFormChange = (prop: keyof State) => (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setConfiguration({ ...configuration, [prop]: event.target.value });
  };

  const handleVisibiliyToggle = (prop: keyof State) => () => {
    setConfiguration({ ...configuration, [prop]: !configuration[prop] });
  };

  const handleMouseDown = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  const validateForm = () => {
    const apiEndpointError = configuration.apiEndpoint === '';

    setConfiguration({ ...configuration, apiEndpointError });

    const errors = apiEndpointError;

    return !errors;
  };

  const submitForm = async () => {
    const formValidated = validateForm();

    if (formValidated) {
      try {
        const res = await fetch(`${configuration.apiEndpoint}`, {
          method: 'POST',
          body: JSON.stringify({
            applicationId: configuration.applicationId,
          }),
          headers: { 'Content-Type': 'application/json' },
        });

        const json = await res.json();

        const { jwt } = json;

        // Store the JWT
        setConfiguration({ ...configuration, apiJwt: jwt });

        closeConfiguration();
      } catch (e) {
        setConfiguration({ ...configuration, submit: false });
        console.log('An error occurred', e);
      }
    }
  };

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    await submitForm();
  };

  useEffect(() => {
    if (configuration.submit) {
      submitForm();
    }
  });

  return (
    <Dialog
      onClose={closeConfiguration}
      aria-labelledby='configuration-dialog-title'
      open={showConfiguration}
      fullWidth
      maxWidth='lg'
    >
      <CustomDialogTitle
        id='configuration-dialog-title'
        onClose={closeConfiguration}
      >
        {t('configuration-title')}
      </CustomDialogTitle>
      <CustomDialogContent dividers>
        <Typography component='span' variant='caption' gutterBottom>
          {t('required-fields')}
        </Typography>
        <Typography gutterBottom>
          <form
            className={classes.form}
            noValidate
            autoComplete='off'
            onSubmit={handleSubmit}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  id='apiEndpoint'
                  name='apiEndpoint'
                  variant='outlined'
                  required
                  fullWidth
                  type='text'
                  onChange={handleFormChange('apiEndpoint')}
                  label={t('api-endpoint-label')}
                  defaultValue={configuration.apiEndpoint}
                  placeholder={t('api-endpoint-placeholder')}
                  error={configuration.apiEndpointError}
                  helperText={
                    configuration.apiEndpointError ? `${t('field-error')}` : ''
                  }
                  InputLabelProps={{
                    shrink: true,
                  }}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position='start'>
                        <Cloud />
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  id='apiJwt'
                  name='apiJwt'
                  variant='outlined'
                  fullWidth
                  disabled
                  type={configuration.showJwt ? 'text' : 'password'}
                  label={t('api-jwt-label')}
                  value={configuration.apiJwt}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  hidden
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position='start'>
                        <LockOpen />
                      </InputAdornment>
                    ),
                    endAdornment: (
                      <IconButton
                        aria-label='toggle jwt visibility'
                        onClick={handleVisibiliyToggle('showJwt')}
                        onMouseDown={handleMouseDown}
                        edge='end'
                      >
                        {configuration.showJwt ? (
                          <Visibility />
                        ) : (
                          <VisibilityOff />
                        )}
                      </IconButton>
                    ),
                  }}
                />
              </Grid>
            </Grid>
          </form>
        </Typography>
      </CustomDialogContent>
      <CustomDialogActions>
        <Button
          type='submit'
          variant='contained'
          fullWidth
          className={classes.submit}
          onClick={submitForm}
        >
          {t('submit')}
        </Button>
      </CustomDialogActions>
    </Dialog>
  );
}

ConfigurationDialog.getInitialProps = async (): Promise<any> => ({
  namespacesRequired: ['common', 'configuration-dialog'],
});

export default withTranslation('configuration-dialog')(ConfigurationDialog);
