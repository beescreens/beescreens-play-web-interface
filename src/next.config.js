const dotenv = require('dotenv');
const dotenvExpand = require('dotenv-expand');

const env = dotenv.config();
dotenvExpand(env);

module.exports = {
  env: {
    BEESCREENS_API_ENDPOINT: process.env.BEESCREENS_API_ENDPOINT,
  },
};
